xorg-advisory-creator
=====================

Templates for creating an X.Org security advisory.

These templates comprise two files: the (hopefully) static template and the 
config file `CVE-2023-1234.yaml` provided by you that contains the data for the
template.

See the `example.yaml` file for details.

Requirements
============

jinja2 and the jinja commandline tool can be installed with:

```
$ pip install jinja2 jinja-cli
```

Building
========

The `Makefile` provided in this repository wraps most of the functionality.
Read that to figure out the various details, otherwise it's:

```
$ make new
CVE number (CVE-YYYY-XXXX): CVE-2023-1234
Edit CVE-2023-1234/CVE-2023-1234.yaml and then run make
```
Now we can edit the `CVE-2023-1234/CVE-2023-1234.yaml` file to supply the information.
Once that's done we can buid the advisory emails:

```
$ make
Advisory emails are available in CVE-2023-1234/
```
And the emails created are:
```
$ ls CVE-2023-1234/
CVE-2023-1234.advisory
CVE-2023-1234.advisory.draft
CVE-2023-1234.advisory.preview
CVE-2023-1234.yaml
```

Note that the Makefile builds the latest `CVE-*` directory only (as judged by
`ls -t`) so if it picks the wrong directory, `touch` should fix that.

The `.advisory` files contain the emails to be sent to
- `.advisory.draft` is the email to be sent to xorg-security
- `.advisory.preview` is the email to be sent to distros@openwall a week before disclosure
- `.advisory` is the final email to be sent to xorg-announce on the disclosure date

Use `mutt -H` to send the file or your favourite email client otherwise.

License
=======

[`SPDX-License-Identifier: MIT`](https://spdx.org/licenses/MIT.html)
