SRCDIR=$(shell ls --directory -t CVE-*/ | head -n 1)
YAMLS = $(shell ls -t $(SRCDIR)/CVE-*.yaml | head -n 1)
ADVISORIES = $(YAMLS:.yaml=.advisory)

%.advisory: %.yaml xorg-advisory.tmpl
	@jinja -d $< --define stage draft < xorg-advisory.tmpl > $@.draft
	@jinja -d $< --define stage preview < xorg-advisory.tmpl > $@.preview
	@jinja -d $< --define stage final < xorg-advisory.tmpl > $@
	@echo "Advisory emails are available in $(shell dirname $@)/"

all: xorg-advisory.tmpl $(ADVISORIES)

clean:
	rm -f CVE-*/*.draft CVE-*/*.preview CVE-*/*.advisory

new:
	@read -p "CVE number (CVE-YYYY-XXXX): " CVE && \
		mkdir -p $${CVE} && \
		cp example.yaml $${CVE}/$${CVE}.yaml && \
		echo "Edit $${CVE}/$${CVE}.yaml and then run make"

help:
	@echo "- Run 'make new' and supply the CVE number in the form CVE-2023-1234."
	@echo "- Edit CVE-2023-1234/CVE-2023-1234.yaml to add the information required."
	@echo "- Run 'make' to build the advisories"

.SUFFIXES: .yaml .advisory .draft .preview

.PHONY: help
