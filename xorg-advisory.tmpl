{#- 
  $ pip install jinja-cli 
  $ jinja -d config.yaml < advisory.tmpl
-#}
{#- ######## List of released tags using oxford commas ########- #}
{%- set tags %}
{%- for project, version in versions.items() -%}
{% if loop.last %} and {% endif %}{{projects[project].tag_prefix}}-{{version}}{% if not loop.last and loop.length > 2%}, {% endif %}
{%- endfor -%}
{%- endset -%}
{######### List of git repos #########}
{%- set _repos = [] -%}
{%- for name, project in projects.items() if name in versions -%}
  {% set tmp = _repos.append(project.repo) %}
{%- endfor -%}
{%- set repos = (_repos | unique | list) -%}
{######### Text for "foo prior to blah"  #########}
{%- set prior_to -%}
{%- for project, version in versions.items() -%}
{{projects[project].name}} prior to {{version}}{% if not loop.last %} and {% endif -%}
{% endfor -%}
{%- endset -%}
{######### Text for project names  #########}
{%- set project_names -%}
{%- for p, project in projects.items() if p in versions -%}
{%- if loop.last %} and {% endif %}{{project.name}}{% if not loop.last and loop.length > 2%}, {% endif -%}
{% endfor -%}
{%- endset -%}
{#- ######## Mail headers ######## -#}
{%- if stage == "draft" -%}
To: xorg-security@lists.x.org
Subject: Please review draft advisory for {{project_names}}

*Under embargo, please keep confidential until {{embargo.date}}*

Please review and provide feedback on the below draft advisory & 
attached patches for {{project_names}} bugs we plan
to publicly disclose on {{embargo.date}} {{embargo.time}}.

A preview of this advisory will be sent to the distros@openwall
mailing list one week before the disclosure date to give the distros
time to prepare and test their fixes.

The patches should apply cleanly to the respective master branches.

------------------------------------------------------------------------

{%- elif stage == "preview" -%}
To: distros@vs.openwall.org
Subject: [vs] Embargoed X.Org Security Advisory: Issues in {{project_names}}
{%- else -%}
To: xorg-announce@lists.x.org, xorg@lists.x.org
Subject: X.Org Security Advisory: Issues in X.Org {{prior_to}}
{% endif %}
{######### Mail content starts here #########}

{%- if stage in ["preview"] -%}Preview of {%endif %}X.Org Security Advisory: {{embargo.date}}

Issues in X.Org {% for project, version in versions.items() -%}
{{projects[project].name}} prior to {{version}}{% if not loop.last %} and {% endif -%}
{% endfor %}
========================================================================
{% if stage in ["preview"] %}
*** Embargoed until {{embargo.date}} {{embargo.time}}
***
*** We plan to publish {{tags}}
*** on {{embargo.date}} when publishing this advisory. Patches are attached, 
*** they apply to the current master branch{% if repos|length > 1 %}es{% endif %} of
*** {{repos|join("\n*** ")}}
{% endif %}
Multiple issues have been found in the {% for project in versions -%}
{% if loop.last %} and {% endif %}{{projects[project].name}}{% if not loop.last and loop.length > 2%}, {% endif %}
{%- endfor %} implementation{% if versions|length > 1 %}s{% endif %} 
published by X.Org for which we are releasing security fixes for in
{{tags}}.
{% for cve in cves %}
{{loop.index}}) {{cve.cve}} {{cve.summary}}
{%- endfor %}
------------------------------------------------------------------------
{% for cve in cves %}
{{loop.index}}) {{cve.cve}}: {{cve.title}}

Introduced in: {{cve.introduced}}
Fixed in: {{tags}}
Fix: {{cve.fix.repo}}-/commit/{{cve.fix.sha}}
Found by: {{cve.found}}

{{cve.details}}
{{tags}} have been patched to fix this issue.

{% endfor -%}
------------------------------------------------------------------------

X.Org thanks all of those who reported and fixed these issues, and those
who helped with the review and release of this advisory and these fixes.
